<?php

namespace App;


use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;


class Main
{

    const DOMAIN = 'https://www.yjc.ir';
    private $client;
    private $crawler;
    private $count;

    public function __construct()
    {
        // config
        $this->client = new Client();
        $guzzleClient = new GuzzleClient(array('timeout' => 60));
        $this->client->setClient($guzzleClient);
        $url = 'https://www.yjc.ir/fa/news/7012639/%D8%B1%DB%8C%D8%B2%D8%B4-%DB%8C%DA%A9-%D9%85%DB%8C%D9%84%DB%8C%D9%88%D9%86%DB%8C-%D9%82%DB%8C%D9%85%D8%AA-%D9%85%D8%AD%D8%B5%D9%88%D9%84%D8%A7%D8%AA-%D8%A7%DB%8C%D8%B1%D8%A7%D9%86-%D8%AE%D9%88%D8%AF%D8%B1%D9%88-%D9%BE%DA%98%D9%88-%DB%B4%DB%B0%DB%B5-glx-%D8%A8%D9%87-%DB%B7%DB%B1-%D9%85%DB%8C%D9%84%DB%8C%D9%88%D9%86-%D9%88-%DB%B5%DB%B0%DB%B0-%D9%87%D8%B2%D8%A7%D8%B1-%D8%AA%D9%88%D9%85%D8%A7%D9%86-%D8%B1%D8%B3%DB%8C%D8%AF';
        $this->crawler = $this->client->request('GET', $url);
    }

    public function run()
    {
        $this->getLatestLinks(10, 10);
        $this->goToFirstLink();
        echo $this->getTitle();
        echo $this->getSubtitle();
        echo $this->getImage();
        $this->getContent();

    }

    private function getLatestLinks(int $from, int $count)
    {
        $from++;
        $this->count = $count;
        $skipFromSelector = ".box_b .t_l_content .linear_news:nth-child(n+$from) a";
        $this->crawler->filter($skipFromSelector)->each(function ($node) {
            if ($this->count === 0) return;
            $this->count--;
            echo "<a href='" . self::DOMAIN . $node->attr('href') . "'>{$node->text()}</a><br/>";
        });
    }

    private function goToFirstLink(): void
    {
        $selector = '.box_b .news_l_column_content .t_l_content .linear_news:first-child a';
        $link = $this->crawler->filter($selector)->link();
        $this->crawler = $this->client->click($link);
    }

    private function getTitle(): string
    {
        $selector = '.news_body_con .title h1';
        return '<h1>' . $this->crawler->filter($selector)->text() . '</h1>';
    }

    private function getSubtitle(): string
    {
        $selector = '.news_body_con .subtitle h2';
        return '<h2>' . $this->crawler->filter($selector)->text() . '</h2>';
    }

    private function getImage(): string
    {
        $selector = '.news_body_con .body p:first-child img';
        return "<img src='{$this->crawler->filter($selector)->image()->getUri()}' /> <br/>";
    }

    private function getContent()
    {
        $selector = '.news_body_con .body p:not(:last-child)';
        $this->crawler->filter($selector)->each(function ($node) {
            echo "<p>{$node->text()}</p>";
        });
    }

}