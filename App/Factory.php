<?php

namespace App;


class Factory
{
    public static function build(string $class,array $args = [])
    {
        return new $class(...$args);
    }
}