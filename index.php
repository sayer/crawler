<?php
require_once 'vendor/autoload.php';

use App\Factory;
use App\Main;

Factory::build(Main::class)->run();

